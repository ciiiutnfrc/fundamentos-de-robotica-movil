#!/usr/bin/env python

"""
Nodo para comandar un robot de tracción diferencial
"""

import rospy
from geometry_msgs.msg import Twist

class Squarepath:
    """ Clase del nodo """

    def __init__(self):
        """ Constructor """

        # Publicador de comandos de velocidad
        self.pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)

        # Se registra la función para fijar v=0 al finalizar
        rospy.on_shutdown(self.stop_robot)

        # Función de ejecución del camino
        self.square_path()

    def square_path(self):
        """ Función de ejecución del camino """

        rospy.loginfo('Ejecutando el camino...')

        # Espera a que se establezca la conexión
        n_sub = self.pub.get_num_connections()
        while n_sub == 0:
            rospy.loginfo('Esperando conexión')
            rospy.sleep(1.0)
            n_sub = self.pub.get_num_connections()

        # Completar el código
        # vel = Twist()
        # vel.linear.x = 0.1
        # vel.angular.z = 0.0
        # self.pub.publish(vel)
        # rospy.sleep(5.0)


        # FIN: fija velocidad 0
        rospy.loginfo('Stop')
        self.pub.publish(Twist())

    def stop_robot(self):
        """ Detiene el robot al finalizar el programa """
        rospy.loginfo('Stop!')
        self.pub.publish(Twist())

if __name__ == '__main__':
    rospy.init_node('square_path')
    Squarepath()
    rospy.spin()
