# Fundamentos de robótica móvil

Gonzalo Perez Paina, David Gaydou y Diego Gonzalez Dondo

## TP3: Mediciones de sensor de barrido láser

### Propuesta
Analizar las mediciones de un sensor de barrido láser y la relación entre los sistemas de coordenadas del sensor/robot y del entorno. 
Para ello se deberá utilizar el robot RoMAA tanto en su versión real como en simulación con Gazebo (ver Fig. 1).
El objetivo del trabajo es generar un mapa a partir de la nube de puntos obtenidas mediante un sensor de barrido láser o LiDAR.

<center>
  <img src="img/romaa_construction_rosds.png" alt="Balancín" width="700"/>
  <br><b>Figura 1.</b> Simulación del robot RoMAA con Gazebo en ROSDS.
</center>
&nbsp;

## Captura y conversión de datos

### Registro (log) de datos
Para obtener el registro de datos o logs tanto del robot real como simulado se utiliza la herramienta de ROS `rosbag`. El comando a utilizar es el siguiente:
```
rosbag record /cmd_vel /odom /scan
```
El registro de datos generado contiene los datos de comandos de velocidad (tópico `/cmd_vel`), de odometría (tópico `/odom`) y mediciones del sensor de barrido láser (tópico `/scan`). Este registro se almacena en un archivo con extensión `.bag` en el directorio donde se ejecuta el comando.

Luego de obtener el registro de datos en formato ROS (extensión `.bag`) se tiene que convetir a formato de texto plano (extensión `.txt`) para facilitar su importación desde los scripts de Python. 

Para la conversión de los datos se deben seguir los siguientes pasos:
1. Descargar y construir el paquete de ROS `rosbag2txt` con el comando:
   ```
   git clone https://github.com/ciiiutnfrc/rosbag2txt.git
   ```
   (tener en cuenta que el paquete se debe descargar en el directorio `src` del espacio de trabajo ROS)

1. Convertir los datos mediante el comando:
   ```
   roslaunch rosbag2txt rosbag2txt.launch bagfile:=<bagfile>
   ```
   donde `<bagfile>` es el archivo con extensión `.bag` a utilizar, por ejemplo:
   ```
   roslaunch rosbag2txt rosbag2txt.launch bagfile:=/home/user/2023-06-08-15-39-51.bag
   ```
   El comando lanza un nodo ROS que lee el registro de datos y crea dos archivos de texto, `odom.txt` y `scan.txt`, con los datos de odometría y del sensor de barrido láser, respectivamente. Los archivos `.txt` se crean en el directorio del paquete.

### Formato de los archivos de datos
Los datos de odometría y sensor de barrido láser se almacenan en archivos de texto separados, `odom.txt` y `scan.txt` respectivamente. Estos archivos guardan los datos como se describe a continuación. 

Por un lado, el archivo de datos de odometrı́a (`odom.txt`) contiene la siguiente información:
```
23.09100  -1.84640  -0.49999  -0.00011  0.08975 -0.00021
23.12500  -1.84334  -0.49999  -0.00011  0.08958 -0.00046
23.15800  -1.84042  -0.50000  -0.00013  0.08755 0.00045
23.19100  -1.83753  -0.50000  -0.00012  0.08824 0.00005
...
```
donde cada línea de texto incluye: timestamp, pose $(x, y, \theta)$ y velocidad lineal y angular $(v, \omega)$ del robot. 

Por otra parte, el archivo de datos del sensor de barrido láser (`scan.txt`) contiene:
```
20.99300  inf inf inf inf inf inf inf 3.00530 2.94810 2.93505 2.93062 ...
21.19300  inf inf inf inf inf inf inf 2.99751 2.94755 2.95040 2.93814 ...
21.39300  inf inf inf inf inf inf inf 2.98891 2.95421 2.93638 2.94163 ...
21.59300  inf inf inf inf inf inf inf 2.99390 2.96727 2.94398 2.90903 ...
...
```
donde cada línea de texto incluye: timestamp, valores de distancia o rango $\{\rho_i, i=1,\cdots,N_l\}$.

Los parámetros del sensor de barrido láser son:
* Ángulo de medición: $\zeta_{min}=-(3/4)\pi$ y $\zeta_{max}=(3/4)\pi$.
* Rango de medición: $\rho_{min}=0.10m$ y $\rho_{max}=10.0m$.
* Cantidad de haces: $N_l=540$ para el robot simulado y $N_L=810$ para el robot real.

## Actividades

### Parte 1: registro de datos de simulación

1. Ejecutar la simulación del robot RoMAA con el sensor de barrido láser en Gazebo y la aplicación de registro de datos:

   1. Ejecutar el simulador del robot RoMAA utilizando la herramienta `roslaunch`:
      ```
      roslaunch romaa_gazebo construction.launch
      roslaunch romaa_gazebo spawn_xacro.launch laser:=1
      rosbag record /cmd_vel /odom /scan
      ```
   1. Mover el robot utilizando el teclado intentando abarcar la mayor parte del escenario de trabajo del robot. Luego detener el registro de datos y los demás programas ejecutados (utilizar Ctrl + C).

   NOTA: Para que el simulador Gazebo pueda cargar los modelos de los objetos incluidos en el escenario se debe clonar el repositorio `gazebo_models` con:
      ```
      git clone https://github.com/osrf/gazebo_models.git
      ```
   y luego agregar la ruta/path del directorio donde se clonó `gazebo_models` a la variable de entorno `GAZEBO_MODEL_PATH`:
      ```
      export GAZEBO_MODEL_PATH=$HOME/<subdirs>/gazebo_models
      ```

1. Convertir los datos almacenados en el archivo de registro (`.bag`) a formato de texto plano (`.txt`), como se indicó anteriormente.

1. Codificar un script en Python que cargue los datos del archivo `scan.txt` y genere gráficos en coordenadas polares de 3 barridos del sensor láser distribuidos de forma uniforme en el tiempo de simulación.

### Parte 2: sincronización de datos
Como es usual, la tasa de muestreo de las mediciones de odometría (pose del robot) y del sensor de barrido láser son diferentes y por lo tanto los timestamps no coinciden. Esto sucede en los datos adquiridos en las simulaciones anteriores, donde la tasa de medición de odometría es mayor que del sensor de barrido láser. Ante esta situación, se pide:

1. Suponiendo que los valores de timestamp de las mediciones de odometría y del sensor de barrido láser se encuentran almacenados en sendos arreglos unidimensionales. Diseñar un algoritmo que determine los índices del vector de timestamp de odometría que se encuentren más próximos a los valores del vector de timestamp de las mediciones del láser.

1. Mostrar en una gráfica los instantes de tiempo (timestamp) de las mediciones del sensor de barrido láser y de odometría. Resaltar aquellos instantes de tiempo de mediciones de odometría más próximas a las del sensor de barrido láser. Mostrar solo una fracción del tiempo total de simulación.

### Parte 3: generación de mapa
Generar un mapa a partir de las mediciones del sensor de barrido láser. Este mapa consiste en graficar las mediciones del sensor de barrido láser (puntos $(x, y)$) obtenidas durante toda la simulación en un sistema de coordenadas común, que en este caso será el de odometría. 

Para esto, escribir un script que genere una gráfica del mapa, el cual tiene que:
1. Convertir las mediciones del barrido láser de coordenadas polares a rectangulares.

1. Transformar los puntos de medición del sensor láser al sistema de coordenadas de odometría y graficar.

1. Graficar junto al mapa el camino seguido por el robot.

Asumiendo que la cantidad de mediciones del sensor láser son menores que las de odometría, la conversión de sistema de coordenadas (láser $\longrightarrow$ odometría) se debe realizar con el valor de odometría más cercano en el tiempo a medición la del láser.

### Parte 4: generación de mapa con el robot RoMAA real

1. Obtener un registro de datos (`.bag`) utilizando el robot RomAA real con el sensor de barrido láser y generar un mapa del entorno.
