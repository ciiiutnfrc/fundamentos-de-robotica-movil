# Fundamentos de robótica móvil

Gonzalo Perez Paina, David Gaydou y Diego Gonzalez Dondo

## TP1: Modelo cinemático y odometrı́a en robot de tracción diferencial

### Objetivos
Comprender el modelo cinemático y la estimación de pose (posición y orientación) mediante el cálculo de odometría de un robot con ruedas de tracción diferencial que se mueve sobre una superficie plana.

### Descripción de la actividad

Las actividades presentadas más adelante consisten en el uso de robots de tracción diferencial en simulación y con un robot real.

Para la parte de simulación se deberá poner en marcha el simulador Gazebo con el modelo de simulación del robot [RoMAA-II](https://ciii.frc.utn.edu.ar/wiki/Robotica/RoMAARobot) bajo el entorno ROS Development Studio ([ROSDS](https://www.theconstructsim.com/)). La siguiente figura muestra una captura de pantalla del entorno ROSDS con el simulador en ejecución.

<div align="center">
  <img src="img/rosds_noetic_romaaii.png" alt="RoMAA-II en Gazebo" width="800"/>
  <br>Simulación del robot RoMAA-II en Gazebo bajo ROSDS.
</div>

<br>Para la puesta en marcha del simulador resolver primero la guía de ejercicios sobre ROS. A partir de la puesta en marcha, se deberán realizar algunos cálculos y scripts para obtener gráficos con el propósito de realizar un análisis del movimiento del robot.

Por otro lado, las actividades que involucran al robot real hacen uso del robot [EduRoMAA](https://github.com/ciiiutnfrc/eduromaa) en la versión BluePill, utilizando el [IDE Arduino](https://www.arduino.cc/en/software) para su [programación](../guias/GuiaProgramacionBluePill.md). Para lo cual se deberá seguir el tutorial para su puesta en marcha. 

<div align="center">
  <img src="img/eduromaa_bluepill.jpeg" alt="EduRoMAA con BluePill" width="450"/>
  <br>Robot con fines didáctico EduRoMAA con BluePill.
</div>

### Actividades

#### Parte 1: cálculos analíticos

  1. Determinar de forma analítica el radio del camino circular que realiza el robot al ajustar la velocidad lineal y angular a valores constantes. Realizar el cálculo para dos velocidades cualquieras teniendo en cuenta las velocidades máximas del robot. <br>**Nota:** Los límites de velocidad y los parámetros cinemáticos (el radio de la rueda $`R`$ y la distancia entre ruedas $`b`$) del robot RoMAA considerar una velocidad máxima de rueda de $`2m/s`$ con $`R=0,140m`$ y $`b=0,450m`$.

  1. Calcular la velocidad lineal y angular para que el robot realice un camino circular con un radio a elección entre 0,5m y 2,0m.

  1. Calcular las velocidades lineales y angulares de las ruedas (izquierda y derecha) del robot para el camino circular del punto anterior.


#### Parte 2: simulación del robot RoMAA en Gazebo

Para los siguientes ejercicios se necesitan los scripts `dump_odom.py` y `square_path.py`que se pueden descargar con:
```
wget https://gitlab.com/ciiiutnfrc/fundamentos-de-robotica-movil/-/raw/main/2023/misc/dump_odom.py
```
y
```
wget https://gitlab.com/ciiiutnfrc/fundamentos-de-robotica-movil/-/raw/main/2023/misc/square_path.py
```

Luego:

  1. Generar un registro (log) de odometría y velocidad del robot con el robot en movimiento mediante teleoperación por teclado. Para ello ejecutar nuevamente la simulación y utilizar el script `dump_odom.py`. Para guardar los datos generados por el script hay que redireccionar la salida a un archivo como:<br>
      ```
      ./dump_odom.py > log.txt
      ```
      Este script `dump_odom.py` muestra en pantalla 6 columnas con los siguientes datos: tiempo (timestamp), coordenadas x, y, orientación, velocidad lineal y angular. Dentro del IDE de ROSDS hay una opción que permite descargar un archivo del proyecto. 

  1. Escribir un script en Python que cargue los datos del archivo de log y genere gráficos de: _i)_ el camino seguido por el robot, _ii)_ la trayectoria (pose respecto al tiempo) y _iii)_ la velocidad del robot respecto al tiempo.<br>**Nota:** Utilizar una relación de aspecto 1:1 para el gráfico del camino.<br>**Sugerencias:** evitar en lo posible las partes del registro que tanga todos los datos iguales a cero.

  1. Obtener otro registro de datos para un camino circular del robot y gráficar el camino y la trayectoria. Marcar tres puntos cualquiera en el gráfico del camino del robot y sus correspondientes puntos en la trayectoria. Elegir puntos que no se correspondan con el inicio o el final del camino.<br>
  
  1. En base a los gráficos anteriores:
    1. ¿Cuáles son los rangos de valores de las coordenadas $`x`$ e $`y`$ y por qué?
    1. ¿Cuál es el rango de valores de la orientación del robot y por qué?

  1. Obtener diferentes registros y gráficos para caminos circulares con diferentes valores (positivos y negativos) de velocidades lineales y angulares (utilizar todas las combinaciones de signos posibles). Indicar en los gráficos el sentido de avance del robot.<br>**Nota:** Graficar únicamente el camino y no la trayectoria.

  1. Describir cuál sería la secuencia de comandos de velocidad a aplicar al robot para seguir uno de los caminos mostrado a continuación (elegir sólo uno).

      <div align="center">
        <img src="img/square_path1.png" width="250" hspace="40"/>
        <img src="img/square_path2.png" width="250" hspace="40"/>
        <br>Caminos cuadrados.
      </div>

  1. Completar el script `square_path.py` para que el robot el robot RoMAA-II simulado en Gazebo pueda ejecutar el camino cuadrado mostrado en el punto anterior.

#### Parte 3: práctica con robot EduRoMAA

Las siguientes actividades consisten en escribir diferentes programas para la BluePill que hagan que el robot se mueva siguiendo diferentes caminos. 
A diferencia del robot simulado, el control del EduRoMAA se realiza a lazo abierto, actuando directamente sobre la señal PWM que accionan los motores.

En el [repositorio](https://github.com/ciiiutnfrc/eduromaa/tree/master/codigo/BluePill) del EduRoMAA se encuentran códigos de ejemplos para los distintos perifericos del robot.

  1. Escribir un programa que mueva al robot una distancia fija hacia adelante y la misma distancia hacia atrás. O sea, el robot se tiene que mover primero con velocidad positiva y luego con velocidad negativa.
  
  1. Escribir un programa que mueva al robot en un camino cuadrado.
  
  1. Responder las siguientes preguntas en base a los ejercicios anterior:
     1. ¿Cuáles son los valores de las acciones de control utilizados?
     1. ¿Qué observa en relación al camino seguido por el robot?
     1. ¿Se detuvo en el mismo punto de partida? 
     1. ¿Sigue el robot los caminos deseados?
     <br>  
  1. Escribir un programa que ejecute los caminos circulares similares a los del punto 4 de la Parte 2. ¿Qué valores de PWM se fijan para cada camino circular?

  
