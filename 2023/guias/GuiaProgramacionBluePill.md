# Programando la BluePill

Existen diferentes herramientas para programar la BluePill. Basicamente se puede programar utilizando:

* el IDE [Arduino](GuiaBluePillArduinoIDE.md),

* STM32Cube el própio IDE que nos ofrece ST: [Tutorial 1: Programando la STM32 Blue Pill con el Cube Ide. Parte 1: Instalación y configuración | profe Tolocka](https://www.profetolocka.com.ar/2021/05/04/programando-la-stm32-blue-pill-con-el-cube-ide-parte-1-instalacion-y-configuracion/),

* en forma BareMetal utilizando bibliotecas y herramientas GNU: 
    1. [Tutorial 1: GFPP](https://github.com/gfpp/STM32F103C8T6_0)
    1. [Tutorial 2: GitHub - andygoulden/baremetal_bluepill: A project to build for the STM32F103C6 &#39;Blue Pill&#39; board with as few dependencies as possible](https://github.com/andygoulden/baremetal_bluepill))
    1. [Tutorial 3: LittleArduinoProjects/ARM/STM32F103C8T6/BareMetal at master · tardate/LittleArduinoProjects · GitHub](https://github.com/tardate/LittleArduinoProjects/tree/master/ARM/STM32F103C8T6/BareMetal),

* [Platformio BluePill F103C8 &mdash; PlatformIO latest documentation](https://docs.platformio.org/en/latest/boards/ststm32/bluepill_f103c8.html).
