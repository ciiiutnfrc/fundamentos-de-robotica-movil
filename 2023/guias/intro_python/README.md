# Introducción a Python

Notebooks de Introducción al lenguaje orientado a objetos Python

  * [Introducción y tipos de datos básicos en python](intro_python1.ipynb)
  * [Organización de paquetes y bibliotecas en python](intro_python2.ipynb)
  * [Operaciones y operadores básicos en python](intro_python3.ipynb)
