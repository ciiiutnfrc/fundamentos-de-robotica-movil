# Guías de estudios
En esta carpeta encontraran las diferentes guías para avanzar con los trabajos prácticos de la materia.

## Guía de python
[Noteboks](intro_python/README.md) de introducción al lenguaje de programación Python.
 
[Guia de ejercios y preguntas básicas sobre python](GuiasPython.md)

## Guia para graficar datos en python
[Guia de ejercios para plotear datos](GuiasPythonPlot.md)

## Guía de ROS
[Guia de ejercios sobre ROS](GuiaROS.md)

## Guía para programar la BluePill 
[Guia para programar la BluePill](GuiaProgramacionBluePill.md)

## Guía JupyterNotebook

