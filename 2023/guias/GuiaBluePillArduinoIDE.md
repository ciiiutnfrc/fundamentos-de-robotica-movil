# Programando la STM32 Blue Pill con Arduino IDE

### Elementos necesarios:

- Placa de desarrollo [STM32 Blue Pill](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill) 
- Programador / debugger [ST-Link V2](https://stm32-base.org/boards/Debugger-STM32F103C8U6-STLINKV2)
- Tira de 4 cables hembra-hembra para conectar la placa al programador
- [Arduino IDE](https://www.arduino.cc/en/software) (Probado con versión 1.8.19) 
- [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) (Probado con versión 2.11.0)  4  (requiere registro para descargar)

-- Probado en Linux Mint 20.3  y Debian -- 

# Una breve reseña de la STM32 Blue Pill

<img title="" src="assets/2023-02-03-12-57-52-image.png" alt="" data-align="center">

La STM32 Blue Pill es una palca de desarrollo basada en un microcontrolador STM32F103C8T6 de la empresa ST-Microelectronics. Se trata de un micro de 32 bits con núcleo Arm Cortex-M3 con una frecuencia máxima de trabajo de 72MHz en un encapsulado LQFP de 48 pines. Su memoria SRAM tiene una capacidad de 20KB, mientras que la memoria FLASH tiene 64KB (aunque se pueden encontrar versiones de 128KB).

<img title="" src="assets/2023-02-03-13-22-42-image.png" alt="" width="257" data-align="center">

                                    [Diagrama en bloques del núcleo arm Cortex-M3 ](https://developer.arm.com/Processors/Cortex-M3)         

        Los principales periféricos del[ micro son:](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html) 

    • Low-power 
        ◦ VBAT supply for RTC and backup registers 
    • 2x 12-bit, 1 μs A/D converters (up to 16 channels) 
        ◦ w/ Temperature sensor 
    • DMA 
        ◦ 7-channel DMA controller 
    • Puertos de E/S rápidos
        ◦ 37 I/Os, all mappable on 16 external interrupt vectors and almost all 5 V-tolerant 
    • Debug mode
        ◦ Serial wire debug (SWD) & JTAG interfaces 
    • 7 timers 
        ◦ 3x 16-bit timers, each with up to 4 IC/OC/PWM or pulse counter and quadrature (incremental) encoder input 
        ◦ 1x 16-bit, motor control PWM timer with dead-time generation and emergency stop 
        ◦ 2x watchdog timers (Independent and Window)        
        ◦ 1x SysTick timer 24-bit downcounter 
    • Up to 9 communication interfaces 
            ◦ 2 x I2C interfaces (SMBus/PMBus) 
            ◦ 3x USARTs (ISO 7816 interface, LIN, IrDA capability, modem control) 
            ◦ 2x SPIs (18 Mbit/s) 
            ◦ 1x CAN interface (2.0B Active) 
            ◦ 1x USB 2.0 full-speed interface       

<img title="" src="assets/2023-02-03-14-26-11-image.png" alt="" data-align="center">

Por otro lado, la placa STM32 Blue Pill se caracteriza por su pequeño tamaño (23mm x 53mm) y ser de color azul (a diferencia de su compañera, la Black Pill, de color negra). A continuación podemos ver las Entradas y Salidas, y los conectores disponibles en la placa:

![](assets/2023-02-03-14-26-50-image.png)

Entradas / salidas y conectores de la placa [STM32 Blue Pill](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill)

<img title="" src="assets/2023-02-03-14-30-08-image.png" alt="" data-align="center">

El modo de arranque se puede seleccionar con los jumpers, lo cual se puede usar para programar la placa de maneras diferentes. Nosotros usaremos el modo de arranque User Flash Memory, para más detalles de los demás modos de programación ver las Referencias: [ProfeTolocka](https://www.profetolocka.com.ar/2021/04/21/programando-la-stm32-blue-pill-con-arduino-ide-parte-3-uso-de-adaptador-ftdi/) y [ElectronLinux](https://electronlinux.wordpress.com/2021/03/10/programar-la-bluepill-stm32-a-traves-del-usb-con-dapboot-y-platformio/)

![](assets/2023-02-03-14-31-04-image.png)

La distribución de pines completas se puede ver en la siguiente imagen. Como se puede apreciar, la mayoría de los pines tiene más de una función.

![](assets/271c3bf335613f06714dba3aad62d406037be5b7.png)

                                                                Pines de la Blue Pill

![](/home/diego/.var/app/com.github.marktext.marktext/config/marktext/images/2023-02-03-14-34-41-image.png)Prestar atención a la tensión de entrada soportada de cada pin, en ciertos casos la tensión máxima es de 5V mientras que en otros es de 3,3V.

La placa tiene un led de color verde conectado al pin PC13 (pin N.º 2) del micro, que podemos usar como testigo o proyectos tipo blinky. El led de color rojo indica la presencia de la alimentación de 3,3V de la placa.

![](assets/2023-02-03-14-35-44-image.png)

Circuito esquemático de los leds de la Blue Pill.

#### Alimentando la STM32 Blue Pill

Para alimentar la placa lo podemos hacer desde el puerto USB con 5V, desde el puerto de programación / depuración con 3,3V o desde algunos pines de conexión (headers) tanto con 5V como con 3,3V, aunque siempre debemos tomar la precaución de usar una y solo una opción de alimentación, ya que sino los dispositivos podrían resultar dañados. En la siguiente imagen se resumen las formas de alimentación:

<img title="" src="assets/2023-02-03-14-36-32-image.png" alt="" width="315" data-align="center">

                                    Opciones de alimentación de la STM32 Blue Pill

Si alimentamos la placa con 5V, ya sea a través del puerto USB o a través de los pines de conexión, la tensión de alimentación del micro, que es de 3,3V, se logra por medio de un regulador que se ubica en la parte trasera, el TX6211B (DE=A1D), con encapsulado SOT23-5 de [5 pines](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill).

<img title="" src="assets/2023-02-03-14-37-46-image.png" alt="" data-align="center">

<img title="" src="assets/2023-02-03-14-37-52-image.png" alt="" data-align="center">

<img title="" src="assets/2023-02-03-14-37-58-image.png" alt="" data-align="center">

                Circuito esquemático de las diferentes opciones de alimetación de la Blue Pill

![](/home/diego/.var/app/com.github.marktext.marktext/config/marktext/images/2023-02-03-14-38-30-image.png)Alimentar la placa solamente por una y solo una de las opciones disponibles, ya que se pueden dañar los componentes. Por ejemplo, si estamos programando la placa, no conectar el puerto USB, o si alimentamos por los pines de conexión, evitar alimentar simultáneamente con 5V y 3,3V.

**Aclaración sobre versiones en el mercado de la STM32 Blue Pill.**

En el mercado nos podemos encontrar con diferentes versiones de esta placa. Versiones clonadas que copian el PCB y sustituyen el micro por un clon cuya denominación puede ser CH32, GD32, APM32 y CS32 entre otras. Incluso se pueden encontrar algunas placas con un micro marcado como STM32 y el logo de la fábrica ST pero que no es original. 
En general estas [placas clonadas](https://www.profetolocka.com.ar/2021/04/12/primeros-pasos-con-la-stm32-blue-pill/#caractersticas) funcionan correctamente en la mayoría de los casos, existiendo algunos inconvenientes documentados al momento de la depuración.

<img title="" src="assets/2023-02-03-14-40-26-image.png" alt="" data-align="center">

                                            [STM32 Blue Pill con un chip clonado](https://hackaday.com/2020/10/22/stm32-clones-the-good-the-bad-and-the-ugly/)

### Configurando el IDE de Arduino

Existen varios métodos para programar la Blue Pill: 

- a través del puerto SWD con un programador ST-Link V2,
- a través del puerto USART1 con un adaptador USB – Serie del tipo FTDI, y
- a través del puerto USB de la placa

En cuanto al software necesario, se puede hacer con el IDE de Arduino, con el programador de STM32CubeProgrammer, con el entorno de desarrollo STM32 Cube IDE, entre otros.

En este documento hemos seleccionado la primera opción, ya que además de programar la placa, podremos depurar los programas. Por lo tanto, para programar la placa usaremos el puerto SWD con un entorno Arduino IDE, aunque necesariamente vamos a tener que instalar el programador STM32CubeProgrammer.

#### Los pasos a seguir son:

- Instalar el[ Arduino IDE](https://www.arduino.cc/en/software) 

- Configurar el Arduino IDE con el core para los microcontroladores STM32.
  
  Abrimos el Arduino IDE y nos vamos al Gestor de tarjetas: 
  `Archivo > Preferencias 
  `y nos vamos a encontrar con el cuadro “Gestor de URLs adicionales de tarjetas”.
  Allí, pegamos:
  `https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json`
  
  Si ya tenemos otra dirección cargada, la agregamos separándola de la anterior con una coma.

![](assets/2023-02-03-15-16-17-image.png)

Pulsamos OK y abrimos el gestor de tarjetas yendo al menú

`Herramientas > Placa > Gestor de tarjetas`

Esperamos a que cargue y buscamos STM32, seleccionamos y ponemos instalar. Comenzará la descarga y luego la instalación del proyecto. Este proceso puede demorar bastante tiempo y se recomienda una buena conexión a internet ya que el proyecto STM32 es bastante pesado.

![](assets/2023-02-03-15-21-37-image.png)

El core STM32 es un desarrollo activo, que cuenta con muchos desarrolladores que incluyen nuevas funciones constantemente. No solo incluye el proyecto para la Blue Pill, sino también para muchas otras placas de desarrollo.
Para una referencia mas completa pueden visitar su Wiki, donde encontraran las placas soportadas, las librerías disponibles y una referencia de la API, para saber cómo debemos acceder al hardware. También pueden acceder a la página de GitHub del proyecto y el foro si tienen la necesidad de realizar alguna consulta. (ver Referencias)

Una vez terminada la instalación, podemos ir a:

`Herramientas > Placa > STM32 Boards groups`

y ver todas cores soportados.

Para el caso particular de la Blue Pill, debemos seleccionar:

`Herramientas > Generic STM32F1 series` 

y luego:

`Herramientas > Board Part Number > BluePill F103C8`

### Instalando el programador STM32CubeProgrammer.

Par ala instalación del programador, debemos descargar desde la página:

[https://www.st.com/en/development-tools/stm32cubeprog.html]([STM32CubeProg - STM32CubeProgrammer software for all STM32 - STMicroelectronics](https://www.st.com/en/development-tools/stm32cubeprog.html))

![](assets/2023-02-03-15-24-46-image.png)

Descomprimimos el archivo .zip en alguna carpeta de nuestro /home (por ejemplo /home/[usuario]/Descargas), y dentro de ella nos encontraremos con un ejecutable llamado 

`SetupSTM32CubeProgrammer-2.11.0.linux`

haciendo doble click sobre él y seguimos los pasos para la instalación. 

<img title="" src="assets/2023-02-03-15-25-17-image.png" alt="" width="471">

La carpeta de instalación por defecto es 

`/home/[usuario]/STMicroelectronics/STM32Cube/STM32CubeProgrammer`

Una vez isntalado debemos agregar los ejecutables a la variable PATH, para que el IDE de Arduino lo pueda llamar sin problemas. Para ello, abrimos un terminal y [ponemos](https://www.sysadmit.com/2016/06/linux-anadir-ruta-al-path.html): 

`export PATH=$PATH:/ruta`

Por ejemplo, para la carpeta de instalación predeterminada, es: export `PATH=$PATH:/home/[usuario]/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin`

Esto lo debemos repetir por cada nuevo inicio de sesión del usuario, por ello, si lo queremos dejar en forma permanente, debemos agregar esa misma línea al final del archivo:

`etc/profile`

lo cual debemos hacer con permisos de administrador, y nos pedirá la contraseña de sudo. Por ejemplo, si lo modificamos con nano es:

`$ sudo nano /etc/profile`

y agregamos al final la línea export y guardamos con ctrl+o

Por último, dependiendo de los permisos de ejecución que tenga el IDE de Arduino, vamos a necesitar darle los permisos para que pueda abrir el puerto USB al que está conectado el ST-Link V2.
Para ello, debemos hacer lo [siguiente](https://stackoverflow-com.translate.goog/questions/22713834/libusb-cannot-open-usb-device-permission-isse-netbeans-ubuntu?_x_tr_sl=auto&_x_tr_tl=es&_x_tr_hl=es-419):

**NOTA  **  Para hacer estos pasos no es necesario que la Blue Pill esté conectado al grabador.

- Primero, verificar qué puerto USB está asignado al ST-Link. Invocamos el comando ls primero con el programador desconectado y luego lo mismo pero con el programador conectado:

`$ ls -l /dev/bus/usb/00*`

y nos debería aparecer algo similar a esto:

![](assets/2023-02-03-15-33-30-image.png)

vamos a ver los permisos otorgados a cada bus y dispositivo conectado. Luego repetimos, pero conectando el programador, y nos aparecerá algo similar a esto:

![](assets/2023-02-03-15-33-50-image.png)

el número de indentificación puede variar, pero generalmente aparecerá al último. Nuetro dispositvo es entonces

`/dev/bus/usb/002/005`

Como vemos, los permisos otorgados al root son de R/W pero para los demás usuarios son de solo lectura (R). Ahora le daremos permiso de escritura para todos los usuarios:

`$ sudo chmod a+w /dev/bus/usb/002/005`

podemos verificar con el comando ls que se otorgaron los permisos de escritura para los demás usuarios. En nuestro ejemplo nos queda (se agregó la w):

![](assets/2023-02-03-15-34-24-image.png)

NOTA    Consulten la referencia, en donde se explican los pasos a seguir para crear un archivo de reglas udev y evitar hacer estos pasos cada vez que quitamos el programador del puerto o reiniciamos la PC. 

### Programando la STM32 Blue Pill: Proyecto blinky

Ya deberíamos estar en condiciones de programar nuestro primer ejemplo, y como más no podía ser, grabaremos un proyecto “blinky” que sería nuestro “hola mundo”.

Con la instalación del STM32CubeProgrammer, ya deberían estar instalados los driver para manejar el ST-Link V2, y los permisos de escritura del puerto fueron otorgados en los pasos indicados anteriormente. Pasemos entonces a la parte eléctrica o conexionado.

Con el cable conectamos el programador y la Blue Pill respetando las 4 conexiones: 3,3V, SWCLK, SWDIO y GND. Notar que no son directas (los cables quedan cruzados) y que los pines de conexión del programador van como se indican en la figura:

<img title="" src="assets/2023-02-03-15-35-11-image.png" alt="" width="323" data-align="center">

<img title="" src="assets/2023-02-03-15-35-32-image.png" alt="" width="208" data-align="center">

Una vez hechas las conexiones, revisamos y si está todo en orden, procedemos a conectar el programador al puerto USB. Con ello se deberían encender los leds rojo y verde de la Blue Pill, este último debería parpadear rápidamente, y además, debería encenderse el led del programador

Abrimos el Arduino IDE y seleccionamos 
`Herramientas > Placa > STM32 boards groups... > Generic STM32F1 series`

y luego:

`Herramientas > Board part number > BluePill F103C8`

por último, debemos asegurarnos que está seleccionado el grabador adecuado:

`Herramientas > Upload method > STM32CubeProgrammer (SWD)`

Por último, cargamos el ejemplo Blink desde 

`Archivo > Ejemplos > 01.Basics > Blink`

Compilamos y subimos el sketch a la Blue Pill.

De fábrica ya nos venía cargado un programa que hacía parpadear el led verde, pero notaremos la diferencia porque el led parpadea más lento (se prende durante 1 seg y se apaga durante 1 seg).

![](assets/2023-02-03-15-36-47-image.png)

![](assets/2023-02-03-15-36-55-image.png)

![](assets/2023-02-03-15-37-05-image.png)

Si todo sale bien, la consola del Arduino IDE nos debería arrojar un resultado como este.

![](assets/2023-02-03-15-37-12-image.png)

Referencias.

https://stm32-base.org/assets/pdf/boards/original-schematic-STM32F103C8T6-Blue_Pill.pdf

https://github.com/stm32duino/wiki/wiki

https://github.com/stm32duino/Arduino_Core_STM32

https://www.stm32duino.com/

https://www.profetolocka.com.ar/2021/04/19/programando-la-stm32-blue-pill-con-arduino-ide-y-st-link/
