# Guía de ejercicios: El Sistema Operativo de Robótica (ROS)

## Puesta en marcha de ROS Development Studio (ROSDS)

  1. Iniciar sesión en ROSDS: [https://www.theconstructsim.com/](https://www.theconstructsim.com/).
  1. Crear un nuevo proyecto ROS (ROSject) y seleccionar ROS Noetic como Distro.
  1. Ejecutar el proyecto.
  1. Abrir una ventana de la Shell (web shell).

## Inspeccionar el valor de variables de entorno usadas por ROS

  * Versión de ROS:
      ```
      echo $ROS_VERSION
      ```
  * Distribución de ROS:
      ```
      echo $ROS_DISTRO
      ```
  * Ruta para buscar los paquetes:
      ```
      echo $ROS_PACKAGE_PATH
      ```
  * Ver todas las variables de entorno ROS:
      ```
      printenv | grep ROS
      ```

## Instalación de paquetes para la simulación del robot RoMAA en Gazebo

  1. Ir al directorio de los archivos fuentes (`src`) del espacio de trabajo (worspace) de ROS que ROSDS crea por defecto para simulación:
      ```
      cd simulation_ws/src
      ```

  1. Clonar los repositorios del robot:
      ```
      git clone https://github.com/ciiiutnfrc/romaa_ros.git
      ```

  1. Construir los paquetes de ROS, para lo cual hay que estar en el directorio del espacio de trabajo de ROS (Nota 1):
      ```
      cd ..
      catkin build
      ```

  1. Agrega los paquetes recién construidos a la ruta de búsqueda de paquetes de ROS:
      ```
      source devel/setup.bash
      ```

  1. Imprimir la variable de entorno que utiliza ROS para buscar los paquetes y verificar que se incluyan las rutas de los paquetes recién construidos (`romaa_description` y `romaa_gazabo`) (Nota 2).


### Notas
  * **Nota 1:** En caso de que aparezca un error que indica que el espacio de trabajo ha sido creado previamente con `catkin_make` borrar los directorios `build` y `devel` y volver a intentar. Utilizar los siguientes comandos:
      ```
      rm -rf build
      rm -rf devel
      ```
      Mensaje de error:
      ```
      The build space at '/home/user/simulation_ws/build' was previously built by 'catkin_make'. Please remove the build space or pick a different build space.
      ```

  * **Nota 2:** Para una mejor visualización de las rutas incluídas en la variable de entorno se puede ejecutar:
      ```
      echo $ROS_PACKAGE_PATH | tr ":" "\n"
      ```

## Ejecución del simulador con el robot RoMAA

  1. Lanzar el simulador Gazebo:
      ```
      roslaunch gazebo_ros empty_world.launch
      ```

  1. Colocar el modelo del robot RoMAA en el escenario simulado. En otra terminal, ejecutar:
      ```
      roslaunch romaa_gazebo spawn_xacro.launch
      ```
      En caso de querer incluir la simulación del sensor de barrido láser montado sobre el robot RoMAA se tiene que fijar el valor del argumento `laser` a `true`, lo cual se puede realizar en la misma línea de comandos:
      ```
      roslaunch romaa_gazebo spawn_xacro.launch laser:=true
      ```

  1. Mover el robot mediante la línea de comandos. En otra terminal ejecutar:
      ```
      rostopic pub -1 /cmd_vel geometry_msgs/Twist '{linear: {x: 0.2}, angular: {z: 0.2}}'
      ```
      o bien:
      ```
      rostopic pub -1 /cmd_vel geometry_msgs/Twist '[0.2, 0, 0]' '[0, 0, 0.2]'
      ```

  1. Terminar el proceso del comando anterior con la combinación de teclas Ctrl+C y ejecutar el comando nuevamente con diferentes valores de velocidad lineal y/o angular.

  1. Teleoperación del robot mediante teclado:
      ```
      rosrun teleop_twist_keyboard teleop_twist_keyboard.py
      ```

## Ejercicios de línea de comando de ROS

  1. Utilizar el comando de ROS (similar al comando cd de Linux) para ir al directorio del paquete `romaa_gazebo`.

  1. ¿Qué archivos y directorios contiene el directorio `romaa_gazebo`?

  1. Ir al directorio `HOME` del usuario (`cd [ENTER]`) y listar el contenido del paquete `romaa_gazebo` utilizando el comando de ROS para tal fin.

  1. ¿Cómo puede saber si este directorio contiene un paquete ROS?

  1. Lanzar el simulador Gazebo con el modelo del robot RoMAA.

  1. Utilizar el comando de ROS para listar los nodos en ejecución, ¿cuáles son?

  1. Utilizar el comando de ROS para listar los tópicos disponibles, ¿cuáles son?

  1. ¿Qué nodo publica el tópico `odom`?

  1. ¿Qué tipo de mensaje lleva el tópico `odom` y cuales son los campos que lo componen?

  1. Ejecutar el comando de ROS que permite ver los mensajes publicados en el tópico `odom`.

  1. Ejecutar el comando de ROS que permite determinar la frecuencia a la que se publican los mensajes del tópico `odom`.

  1. Ejecutar el comando que permita ver el campo `position` del tópico `odom` (posición x, y y z).

  1. Detener el simulador Gazebo y relanzar la simulación del robot RoMAA con el sensor de barrido láser. Repetir los puntos 8 al 11 para el tópico `scan`.
