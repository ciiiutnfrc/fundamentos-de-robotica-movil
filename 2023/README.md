# Fundamentos de Robótica Móvil 2023

En esta carpeta encontraran el material para realizar las distintas actividades de la material

 * En [guias](guias/README.md) encontraran las guías de ejercios sobre diferentes temas de la materia.
 * En [tps](tps/README.md) se encuentran los trabajos prácticos. 
 * En [misc](misc/README.md) hay diferentes herramientas útiles para realizar los Tps. 
