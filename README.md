# Fundamentos de Robótica Móvil

Repositorio de la materia electiva _"Fundamentos de Robótica Móvil"_ de la Carrera de Ingeniería Elctrónica de la Facultad Regional Córdoba, Universidad Tecnólogica Nacional (UTN-FRC).

**Página oficial de la materia:**

https://www.profesores.frc.utn.edu.ar/electronica/fundamentosroboticamovil/

En este repositorio econtraran materiales útiles de la materia.

### Docentes:

  * Gonzalo Perez Paina
  * David Gaydou
  * Diego Gonzalez Dondo

### Materiales de la materia

En esta carpeta encontraran los materiales de la materia para el año [2023](2023/README.md)
